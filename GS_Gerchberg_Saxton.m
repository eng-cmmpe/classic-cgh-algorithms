%%% Written for CMMPE by Jinze Sha (js2294@cam.ac.uk)
%%% Copyright 2022-2023

close all;
clear;
clc;

%% Set target image file
ImageFileName = "mandrill_2";
ImageFileExtension = ".bmp";
ImageFileNameFull = ImageFileName + ImageFileExtension;

%% Set parameters (for Fresnel propagation)
% distance = 0.25;
% lambda = 532e-9;
% pitch = 13.6e-6; %0.00000425;

%% Load and process target image file
ImageFileRead = imread(ImageFileNameFull);
% Check the number of channels of the target file, which should be 8 bit grayscale
if size(ImageFileRead, 3) == 1
    TargetImage = double(ImageFileRead) ./ 255;
elseif size(ImageFileRead, 3) == 3
    warning("The target image has 3 channels, converting to grayscale by default")
    TargetImage = double(rgb2gray(ImageFileRead)) ./ 255;
else
    error("The target image should have 1 channel, please check its bit depth")
end

%%% The following code might be useful, uncomment to use
% Image resize
% TargetImage = imresize(TargetImage, [1024 1280], "bilinear");

% Horizontal flip
% TargetImage = flipdim(TargetImage, 2);

% Image normalization
% TargetImage = abs(ifft2(real(fft2(TargetImage))));
% TargetImage = TargetImage/max(TargetImage(:));

% Add upside down replica below the target
% T_rot = rot90(TargetImage, 2);
% TargetImage = [T_rot; TargetImage];
%%%

TargetAmplitude = sqrt(TargetImage); % target amplitude is the square root of intensity

%% Create output file folder
if not(isfolder("GS_output"))
    mkdir("GS_output")
end
imwrite(TargetAmplitude, "GS_output/TargetAmplitude.bmp",'bmp');

%% Major loop for hologram generation
tic
N = 30; % Total number of iterations

% Set the initial E as T with random phase
E = TargetAmplitude .* exp(1i*rand(size(TargetAmplitude))*2*pi);

GS_NMSE_list = []; % Record NMSE (for plotting)
for iter = 1:N
    %% Propagate from replay field to hologram aperture
    A = ifftshift(ifft2(ifftshift(E)));
    % A = BackwardPropergation(E,pitch,lambda,distance); %(for Fresnel propagation)

    %% Apply phase-only constraint for hologram aperture
    A = exp(1i*angle(A));
    % Use one of below for binary phase quantization (optional)
    % A = exp(1i * pi * 1./(1+exp(-angle(A)))); % Sigmoid function
    % A = exp(1i * pi * double(angle(A) > 0)); % Rounds all positive phases to pi and negative to 0, does not work with GS
    % A = exp(1i * pi * double(angle(A) > pi/2 | angle(A) < -pi/2)); % Rounds -pi/2 ~ pi/2 to 0 and otherwise to pi, this one works

    %% Propagate from hologram aperture to replay field
    E = fftshift(fft2(fftshift(A)));
    % E = ForwardPropergation(A,pitch,lambda,distance); %(for Fresnel propagation)

    % Save progress (for plotting)
    R = abs(E * sqrt(sum(TargetAmplitude(:).^2) / sum(abs(E(:)).^2))).^2;
    GS_NMSE_list(iter) = immse(R, TargetImage) / sum(TargetImage(:).^2);
    imwrite(angle(A), "GS_output/GS_holo_i_" + string(iter) +".bmp")
    imwrite(R, "GS_output/GS_recon_i_" + string(iter) +".bmp")

    %% Apply the target amplitude constraint at the replay field
    E = TargetAmplitude .* exp(1i * angle(E));
end
toc
%% Save final hologram and reconstruction
imwrite(angle(A), "GS_output/GS_Holo_" + ImageFileName + ".png")
imwrite(R, "GS_output/GS_Recon_" + ImageFileName +".png")

% show final NMSE and SSIM values
immse(R, TargetImage) / sum(TargetImage(:).^2)
ssim(R, TargetImage)

%% Show NMSE plot
plot(GS_NMSE_list)
ylabel("NMSE")
xlabel("iteration (n)")
title("GS algorithm")