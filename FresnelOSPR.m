%%% Written for CMMPE by Daoming Dong, Youchao Wang and Jinze Sha
%%% Copyright 2018-2023

close all;
clear;
clc;

%% Set projector properties here (SLM pitch size and Laser wavelength)
pitch  = 13.6e-6; %6.4000e-06
lambda = 532e-9;  %6.3280e-07

%% Set list of images and their according distances here
image_file_names = {'Target_field_0.png', 'Target_field_1.png', 'Target_field_2.png'};
distances = [1.1, 1.2, 1.3];

%% Create output file folder for OSPR
if not(isfolder("OSPR_output"))
    mkdir("OSPR_output")
end

%% Major loop for hologram generation
N = 8;
% Assumming we want to output three channels in the Freeman projector
for RGB_channel_index = 1:3
    FreemanHoloPerChannel = 0;
    for num_frame = 1:N
        for slice_index = 1: length(image_file_names)
            %% Load and process the target image file for this slice 
            % (images are loaded slice by slice to avoid memory blowing up when too many slices are given)
            ImageFileRead = imread(image_file_names{slice_index});
            % Check the number of channels of the target file, which should be 8 bit grayscale
            if size(ImageFileRead, 3) == 1
                TargetImage = double(ImageFileRead) ./ 255;
            elseif size(ImageFileRead, 3) == 3
                warning("The target image has 3 channels, converting to grayscale by default")
                TargetImage = double(rgb2gray(ImageFileRead)) ./ 255;
            else
                error("The target image should have 1 channel, please check its bit depth")
            end
            
            %%% The following code might be useful, uncomment to use
            % Image resize
            % TargetImage = imresize(TargetImage, [1024 1280], "bilinear");
            
            % Horizontal flip
            % TargetImage = flip(TargetImage, 2);
            
            % Image normalization
            % TargetImage = abs(ifft2(real(fft2(TargetImage))));
            % TargetImage = TargetImage/max(TargetImage(:));
            
            % Add upside down replica below the target
            % T_rot = rot90(TargetImage, 2);
            % TargetImage = [T_rot; TargetImage];
            %%%
            
            T = sqrt(TargetImage); % target amplitude is the square root of intensity
            
            %% Add random phase to the target
            E = T .* exp(1i * 2 * pi * rand(size(T)));

            %% Propagate from target replay field to hologram plane
            A = BackwardPropergation(E, pitch, lambda, distances(slice_index));

            %% Energy conservation
            A = A/sqrt(sum(abs(A(:)).^2)/sum(abs(E(:)).^2));

            %% Sum the holograms
            if slice_index == 1
                A_total = A;
            else
                A_total = A_total + A;
            end
        end

        %% This part is the same as OSPR
        % Apply the Phase-only constraint
        H = angle(A_total);

        % Binary phase quantization
        H = double(H > 0);

        % Freeman projector encoding
        FreemanHoloPerChannel = FreemanHoloPerChannel + H .* 2^(num_frame - 1);

        % Calculate total reconstruction (for debug purpose)
        for slice_index = 1: length(image_file_names)
            R = abs(ForwardPropergation(exp(1i * H * pi), pitch, lambda, distances(slice_index)));
            R = R/sqrt(sum(abs(R(:)).^2)/sum(abs(T(:)).^2));
            if (RGB_channel_index == 1) && (num_frame == 1)
                R_total(:,:,slice_index) = R;
            else
                R_total(:,:,slice_index) = R_total(:,:,slice_index) + R;
            end
        end
    end
    FreemanHolo(:,:,RGB_channel_index) = uint8(FreemanHoloPerChannel);
end

%% Save average reconstruction of all hologram subframes (for debug purpose)
for slice_index = 1: length(image_file_names)
    imwrite(R_total(:,:,slice_index)/3/N/max(max(T)), "OSPR_output/Freeman_OSPR_ReconAvg_d" + distances(slice_index) + ".bmp",'bmp');
end

%% Save the hologram encoded for the Freeman projector
imwrite(FreemanHolo, "OSPR_output/Freeman_OSPR_Holo_Fresnel_d_" + distances(1) + ".bmp",'bmp');
