# Classic CGH Algorithms
This code repository contains MATLAB code for the classic CGH algorithms including DBS, SA, OSPR, ADOSPR, GS and useful functions such as Fresnel propagation and multi-slice Fresnel OSPR.

All codes in this repository are supposed to be simple to run and easy to read, pressing the 'run' button in any MATLAB script file starting with the algorithm names should run the respective algorithms with the sample images straight away.

If you see anything that doesn't look right or any comment that is not clear enough, please feel free to make modifications so that students joining later can have a better experience using this code.
