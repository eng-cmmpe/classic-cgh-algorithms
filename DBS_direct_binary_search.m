%%% Written for CMMPE by Jinze Sha (js2294@cam.ac.uk)
%%% Copyright 2022-2023

close all;
clear;
clc;

rng(1); % manual random seed for consistent results between runs

%% Set target image file
ImageFileName = "test_128";
ImageFileExtension = ".bmp";
ImageFileNameFull = ImageFileName + ImageFileExtension;

%% Load and process target image file
ImageFileRead = imread(ImageFileNameFull);
% Check the number of channels of the target file, which should be 8 bit grayscale
if size(ImageFileRead, 3) == 1
    TargetImage = double(ImageFileRead) ./ 255;
elseif size(ImageFileRead, 3) == 3
    warning("The target image has 3 channels, converting to grayscale by default")
    TargetImage = double(rgb2gray(ImageFileRead)) ./ 255;
else
    error("The target image should have 1 channel, please check its bit depth")
end

%%% The following code might be useful, uncomment to use
% Image resize
% TargetImage = imresize(TargetImage, [1024 1024], "bilinear");

% Horizontal flip
% TargetImage = flipdim(TargetImage, 2);

% Image normalization
% TargetImage = abs(ifft2(real(fft2(TargetImage))));
% TargetImage = TargetImage/max(TargetImage(:));

% Add upside down replica below the target
% T_rot = rot90(TargetImage, 2);
% TargetImage = [T_rot; TargetImage];
%%%

TargetAmplitude = sqrt(TargetImage); % target amplitude is the square root of intensity

%% Create output file folder
if not(isfolder("DBS_output"))
    mkdir("DBS_output")
end
imwrite(TargetAmplitude, "DBS_output/TargetAmplitude.bmp",'bmp');


%% Major loop for hologram generation
N = 100000; % Total number of iterations
T = TargetAmplitude;

% Generate initial binary phase hologram H
H = round(rand(size(T)));

% Compute reconstruction and the initial loss
E = fftshift(fft2(fftshift(exp(1j * H * pi))));
E = E * sqrt(sum(T(:).^2) / sum(abs(E(:)).^2)); %conservation of energy
R = abs(E).^2; %reconstruction intensity is the square of amplitude
L = immse(R, TargetImage);

DBS_NMSE_list = []; % Record NMSE (for plotting)
for n = 1:N
    % Flip random pixel
    random_location = [randi([1 size(T, 1)]) randi([1 size(T, 2)])];
    H_n = H;
    H_n(random_location(1), random_location(2)) = 1 - H_n(random_location(1), random_location(2));

    % Calculate the loss function for the new hologram
    E_n = fftshift(fft2(fftshift(exp(1j * H_n * pi))));
    E_n = E_n * sqrt(sum(T(:).^2) / sum(abs(E_n(:)).^2)); %conservation of energy
    R_n = abs(E_n).^2;
    L_n = immse(R_n, TargetImage);

    % Compare new loss with the old one
    if L_n < L
        % Accept the new hologram if the loss is lower
        H = H_n;
        R = R_n;
        L = L_n;
    end

    % Save current progress
    n, L
    DBS_NMSE_list(n) = L;
    if rem(n, 10000) == 0
        imwrite(H, "DBS_output/DBS_holo_" + string(n) + ".bmp")
        imwrite(R, "DBS_output/DBS_recon_" + string(n) + ".bmp")
    end
end

DBS_NMSE_list = DBS_NMSE_list ./ sum(TargetImage(:).^2);

%% Save final hologram
imwrite(H, "DBS_output/DBS_" + ImageFileName + "_Holo.png")
imwrite(R, "DBS_output/DBS_" + ImageFileName + "_recon_intensity.png")

%% Print final metrics
immse(R, TargetImage) / sum(TargetImage(:).^2)
ssim(R, TargetImage)

%% Show NMSE plot
plot(DBS_NMSE_list)
ylabel("NMSE")
xlabel("iteration (n)")
title("DBS algorithm")