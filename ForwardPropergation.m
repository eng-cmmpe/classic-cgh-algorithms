function [u2] = ForwardPropergation(u1,pitch,lambda,z)
%========================================================================
%Copyright(c) 2019 Fan Yang project
%University of Cambridge 
%All Rights Reserved.
%
%This is an implementation of the Reconstructing holograms by Fresnel Transform Method.
%Assumes x and y are equal to holo_width.
%Input : u1     - Hologram Plane Field
%        pitch  - Pixel Pitch
%        lambda - Wavelength
%        z      - Propagation Distance
%Output: u2     - Object Plane Field
%========================================================================

holo_width = size(u1,2);
holo_height = size(u1,1);
[xx, yy] = meshgrid(1:holo_width, 1:holo_height);
xMeters = pitch*(xx-(holo_width+1)/2);
yMeters = pitch*(yy-(holo_height+1)/2);
zern3 = pi*(xMeters.^2 + yMeters.^2);
h = (-1i)*exp(-1i/(lambda.*z).*zern3);
U2=h.*u1;
u2=fftshift(fft2(U2));	
end 

