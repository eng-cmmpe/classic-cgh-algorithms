%%% Written for CMMPE by Jinze Sha (js2294@cam.ac.uk)
%%% Copyright 2022-2023

close all;
clear;
clc;

%% Set target image file
ImageFileName = "mandrill";
ImageFileExtension = ".tiff";
ImageFileNameFull = ImageFileName + ImageFileExtension;

%% Load and process target image file
ImageFileRead = imread(ImageFileNameFull);
% Check the number of channels of the target file, which should be 8 bit grayscale
if size(ImageFileRead, 3) == 1
    TargetImage = double(ImageFileRead) ./ 255;
elseif size(ImageFileRead, 3) == 3
    warning("The target image has 3 channels, converting to grayscale by default")
    TargetImage = double(rgb2gray(ImageFileRead)) ./ 255;
else
    error("The target image should have 1 channel, please check its bit depth")
end

%%% The following code might be useful, uncomment to use
% Image resize
% TargetImage = imresize(TargetImage, [1024 1280], "bilinear");

% Horizontal flip
% TargetImage = flipdim(TargetImage, 2);

% Image normalization
% TargetImage = abs(ifft2(real(fft2(TargetImage))));
% TargetImage = TargetImage/max(TargetImage(:));

% Add upside down replica below the target
% T_rot = rot90(TargetImage, 2);
% TargetImage = [T_rot; TargetImage];
%%%

TargetAmplitude = sqrt(TargetImage); % target amplitude is the square root of intensity

%% Create output file folder
if not(isfolder("Naive_output"))
    mkdir("Naive_output")
end
imwrite(TargetAmplitude, "Naive_output/TargetAmplitude.bmp",'bmp');

%% Main section for hologram generation
T = TargetAmplitude .* exp(1i * 2 * pi * rand(size(TargetAmplitude)));
A = ifftshift(ifft2(ifftshift(T)));
A = A * sqrt(sum(T(:).^2) / sum(A(:).^2)); % energy conservation

figure
imshow(abs(A), [0 1])
title("Amplitude of hologram which is discarded")
imwrite(abs(A), "Naive_output/Naive_discarded_amplitude.png")

figure
imshow(angle(A), [-pi pi])
title("Phase of hologram which is kept")

H = angle(A);
% H = pi * double(H>0); % binary quantisation
imwrite(H, "Naive_output/Naive_Holo.png")

A = exp(1i .* H); % apply the phase-only constraint
E = fftshift(fft2(fftshift(A)));
E = E * sqrt(sum(T(:).^2) / sum(abs(E(:)).^2)); % energy conservation

R = abs(E) .^ 2;

figure
imshow(R)
title("Reconstruction intensity of the phase-only hologram")
imwrite(R, "Naive_output/Naive_Recon.png")

immse(R, TargetImage) / sum(TargetImage(:).^2)
ssim(R, TargetImage)



